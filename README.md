# steps to assighn SSH TO GITLAB from your pc

this is the link   https://www.genuinecoder.com/how-to-fix-permission-denied-publickey-issue-in-gitlab/

=========================================================================================================

Steps to add SSH key in Gitlab
Run CMD/Powershell/Terminal with administrative (sudo) privilege. (In windows run cmd as administrator. In linux execute ‘sudo su’ to get root privilege).
Type ssh-keygen.
You will see the following. Here you will be asked for the location where the SSH key will be saved. Press enter to accept default or enter your custom location.
Generating public/private rsa key pair.     
Enter file in which to save the key (C:\Users\yourUsername/.ssh/id_rsa):
Git will ask you to save the key to the specific directory.You will be asked for a password. Make sure you remember it since it will be needed for cloning.
Enter passphrase (empty for no passphrase):
The public key will be created to the specific directory.
Now go to the directory you have specified in Step 2 and open .ssh folder.
You’ll see a file id_rsa.pub. Open it on notepad. Copy all text from it.
Go to https://gitlab.com/profile/keys
Here you can see all the SSH keys specified so far. Paste the copied key.

Now click on the “Title” below. It will automatically get filled based on the value taken from the SHA Key.
Then click “Add key” and that’s it. You have successfully configured SSH.
Now try cloning again. Git will ask for a password. Give the password you have given in Step 2.
And that’s all. Now you will be able to access the repo without any issues.